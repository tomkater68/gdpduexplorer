# gdpduexplorer - Betrachter für GDPdU-Dateien #

Die *Grundsätze zum Datenzugriff und zur Prüfbarkeit digitaler Unterlagen (GDPdU)* enthalten Regeln zur Aufbewahrung digitaler 
Unterlagen der Steuerpflichtigen bei Betriebsprüfungen. Dabei werden dem Betriebsprüfer die Daten in einem standardisierten Format zur 
Prüfung vorgelegt. Die Daten werden dabei in einer oder mehreren CSV-Dateien abgelegt. Eine weitere Datei im XML-Format enthält das 
Inhaltsverzeichnis (index,xml) und die Beschreibung der Daten. 

### Was macht das Programm? ###

Dieses Programm liest die index.xml einer GDPdU-Ausgabe und bereitet die enthaltenen Daten anhand dieser Informationen in tabellarische Form auf. 
Nach dem Einlesen der Beschreibungsdatei wird zunächst eine Übersicht mit den vorhandenen Tabellen angezeigt.

![Screenshot01.png](https://bitbucket.org/repo/KEjnb5/images/154659244-Screenshot01.png)

Möchte man sich die Daten in einer Tabelle anschauen, öffnet man mit einem Doppelklick die entsprechende Zeile in der Tabelle und 
öffnet damit die Datenansicht.

![Screenshot02.png](https://bitbucket.org/repo/KEjnb5/images/894080455-Screenshot02.png)

Klickt man in der Datenansicht auf eine Tabellenspalte, werden - sofern in der Beschreibungsdatei vorhanden - zusätzliche Informationen
zu der Tabellenspalte angezeigt. Tabellenspalten, die in der Beschreibung als Primärschlüssel definiert sind, werden in der Datenansicht
rot angezeigt. Tabellenspalten die als Fremdschlüssel markiert sind, werden grau hinterlegt. Zusätzlich wird bei einem Klick auf eine 
Fremdschlüsselspalte angezeigt, welche Tabelle durch den Fremdschlüssel referenziert wird.

Die mit einem Fremdschlüssel verknüpften Daten kann man sich anzeigen lassen, in dem man in der Fremdschlüsselspalte den gewünschten 
Datensatz markiert und den Schalter 'Verknüpfte Daten anzeigen' drückt.

Umgekehrt kann man sich zu einem Primärschlüssel alle Daten anzeigen lassen, die diesen Schlüssel über einen Fremdschlüssel 
referenzieren. Dazu markiert man in einer Primärschlüsselspalte den entsprechenden Datensatz und drückt den Schalter den Schalter 
'Verknüpfte Daten anzeigen'.

![Screenshot03.png](https://bitbucket.org/repo/KEjnb5/images/3669328369-Screenshot03.png)

Ein einfacher Abfrageeditor erlaubt außerdem die Ausführung eigener Abfragen auf den eingelesen Datenbestand. Die Abfragen erfolgen auf 
Basis von SQL. Unterstützt wird der Funktionsumfang von [SQLite](https://sqlite.org/lang.html) (mit Ausnahme von create, alter, insert,
update, replace und drop).

![Screenshot_Abfrageeditor.png](https://bitbucket.org/repo/KEjnb5/images/3144472121-Screenshot_Abfrageeditor.png)

## Download und Installation ##
### Windows ###

Für die Installation unter Windows steht ein Installer zur Verfügung, der [hier](https://bitbucket.org/tomkater68/gdpduexplorer/downloads) heruntergeladen werden. 

### Installation unter [Arch-Linux](http://www.archlinux.de) ###

Für die Installation unter Arch-Linux steht im [Downloadbereich](https://bitbucket.org/tomkater68/gdpduexplorer/downloads) die Datei PKGBUILD zum Download bereit, mit deren Hilfe der Quellcode geladen und compiliert und abschließend ein Installationspaket erstellt werden kann. Gehen Sie dazu folgendermaßen vor:

* Starten Sie den Download der Datei *PKKBUILD*
* Entpacken Sie das Archiv in ein beliebiges Verzeichnis
* Wechseln Sie in dieses Verzeichnis und geben Sie das Kommando *makepkg* ein
* Installieren Sie das erstellte Paket mit *pacman -U <paketname>.pkg.tar.gz*

### Installation unter anderen Linuxversionen ###

Für die Installation unter anderen Linuxversionen muss das Programm aus den Quellen installiert werden. Dafür werden folgende Tools benötigt, die über das Paketmanagement der Distribution installiert werden:

* gcc-Compiler
* git
* Qt ab Version 5.6
* cmake

Zunächst wird der Quellcode mit Hilfe von git geladen:

git clone https://tomkater68@bitbucket.org/tomkater68/gdpduexplorer.git

Wechseln Sie in das Verzeichnis *kvbuch*. Legen Sie dort ein neues Verzeichnis ( z.B. *build* an und wechseln
Sie in dieses Verzeichnis. Geben Sie dort *-DCMAKE_INSTALL_PREFIX=/usr ../* ein. Anschließend starten Sie den Compiliervorgang durch Eingabe von *make*. Nach dem Abschluss des Übersetzungsvorgangs erstellt das Kommando 
*make package* ein Installationspaket mit dem Sie das Programm durch entpacken des Archivs installieren können.

