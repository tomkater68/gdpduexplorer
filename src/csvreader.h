/*
    gdpduexplorer cvsreader.h
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <QObject>

class Backend;
class GDPdU;
class Media;
class Table;

/*!\brief Read table data from csv file */
class CsvReader : public QObject
{
    Q_OBJECT
public:
    /*!
        Initialize object
        \param parent pointer to Backend object
        \param parent pointer to GDPdU object
    */
    explicit CsvReader( Backend* backend, GDPdU* gdpdu );

    bool readCSV( int mediaIndex, int tableIndex );

private:
    Table* getTableFromIndex( int mediaIndex, int tableIndex );

    /*!
        Process imported data
        \param record Raw record data
    */
    void processRecord( QString record );

    /*!
        Parse records with fixed column length
        \param record Raw record data
    */
    void processFixedLength( QString record );

    /*!
        Parse records with vairable column length
        \param record Raw record data
    */
    void processVariableLength( QString record );

    void clearColumnValues();

    /*!
        Zeichenkette gemäß der Beschreibung formatieren
        \param colum Index der zu fomratierenden Spalte
        \param value zu formatierende Zeichenkette
    */
    QString formatString( int column, QString value );

    /*!
        Datum gemäß der Beschreibung formatieren
        \param colum Index der zu fomratierenden Spalte
        \param value zu formatierende Zeichenkette
    */
    QString formatDate( int column, QString value );

    /*!
        Zahlenwert gemäß der Beschreibung formatieren
        \param colum Index der zu fomratierenden Spalte
        \param value zu formatierende Zeichenkette
    */
    QString formatNumeric( int column, QString value );

    Backend* backend = nullptr; ///<pointer to Backend object
    GDPdU* gdpdu = nullptr; ///<pointer to GDPdU object
    Table *table = nullptr; ///<pointer to Table object
    qint64 rowCount = 0; ///CSV file row counter
    qint64 errorCount = 0; ///CSV file error counter
    int errorHandling = 1; ///<Import error handling
    bool importError = false; ///<flag, if import error occured
    bool cancelImport = false; ///flag, if import should be canceled
};
