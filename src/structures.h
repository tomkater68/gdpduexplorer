/*
    gdpduexplorer structures.h
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <QVector>
#include <QString>

/*!\brief Struktur für die Auflösung von Mappings*/
struct Map
{
    QString from = QString();  ///<Quellwert
    QString to = QString();   ///<Zielwert

};

/*!\brief Struktur für die Speicherung von Spalten*/
struct Column
{
    /*!\brief Aufzählung möglicher Datentypen*/
    enum ColumnType
    {
        eAlphanumeric,       ///<Alphanumerisch
        eNumeric,            ///<Numerisch
        eDate                ///<Datumsfeld
    };

    QString name = QString();                   ///<Spaltenname
    QString desc = QString();                   ///<Spaltenbeschreibung
    int type = eAlphanumeric;                   ///<Spaltentyp
    int maxLength = 0;                          ///<Maximale Länge der Spalte
    int accuracy = 0;                           ///<Anzahl der Nachkommastellen
    int fixedFrom = 0;                          ///<Startposition bei Spalten mit fixer Länge
    int fixedTo = 0;                            ///<Endeposition bei Spalten mit fixer Länge
    int fixedLength = 0;                        ///<column length
    int impliedAccuracy = 0;                    ///<Implizite Anzahl von Nachkommastellen
    bool isPrimaryKey = false;                  ///<Feld ist Primärschlüssel
    bool isForeignKey = false;                  ///<Feld ist Fremdschlüssel
    QString foreignKeyReference = QString();    ///<Tabelle, auf die der Fremdschlüssel verweist
    bool hasMap = false;                        ///<Flag, ob die Spalte ein Mapping enthält
    QVector<Map*> mapList;                      ///<Liste vom Typ Map für die Auflösung des Mappings
    QString dateFormat = "dd.MM.yyyy";          ///Datumsformat
    int epoch = 30;                             ///<Jahrhunderttrenner
};

/*!\brief Struktur für die Speicherung von Tabellen*/
struct Table
{

    /*!\brief Enumaration der Zeichensätze*/
    enum codecType
    {
        eANSI,          ///<ANSI-Zeichensatz
        eMacintosh,     ///<Macintosh Zeichensatz
        eOEM,           ///<IBM Ascii
        eUTF16,         ///<UTF16
        eUTF7,          ///<UTF7
        eUTF8           ///<UTF8
    };

    int tableNo = 0;                                ///<Interne Tabellennummer
    QString tableName = QString();                  ///<Name der Tabelle
    QString tableDesc = QString();                  ///<Tabellenbeschreibung
    QString url = QString();                        ///<Name der CSV-Datei mit den Tabellendaten
    QString urlFullPath = QString();                ///<Pfad zur CVS-Datei mit den Tabellendaten
    QString from = QString();                       ///<Von-Datum (Rohformat)
    QString to = QString();                         ///<Bis-Datum (Rohfromat)
    QString dateFormat = "dd.MM.yyyy";              ///<Datumsformat
    QString dateFormatted = QString();              ///<Von-Bis-Datum formatiert
    int codec = eANSI;                              ///<Zeichensatzkodierung
    QString decimalSymbol = ",";                    ///<Dezimaltrenner
    QString digitGrouping = ".";                    ///<Tausendertrenner
    int skipBytes = 0;                              ///<Anzahl Bytes, die  beim Lesen der Datendatei ignoriert werden sollen
    int rangeFrom = 0;                              ///<Index des ersten Datensatzes
    int rangeTo = -1;                               ///<Index des letzten Datensates (-1: alle Sätze lesen)
    int rangeLength = -1;                           ///<Datensatzlänge (-1: alle Daten lesen)
    QString textEncapsulator = "\"";                ///<Marker für Beginn und Ende von Texten
    QString recordDelimiter = QString( "\r\n" );    ///<Zeilentrenner
    QString columnDelimiter = ";";                  ///<Spaltentrenner
    bool isFixedLength = false;                     ///<Flag für fixe Spaltenlänge
    int fixedLength = 0;                            ///<Fixe Länge
    int epoch = 30;                                 ///<Jahrhunderttrenner
    bool imported = false;                          ///<Flag, ob die Daten aus der CSV-Datei bereits gelesen wurden
    QVector<Column*> columnList;                    ///<column list
    QVector<QVector<QString>> columnValueList;      ///Vektor mit den aus der CSV-Datei importierten Daten
};

/*!\brief Struktur für die Speicherung von Medien*/
struct Media
{
    int mediaNo = 0;                                ///<Interne Nummer des Mediums
    QString name;                                   ///<Name des Mediums
    QVector<Table*> tableList;                      ///<Liste vom Typ Table mit den im Meidum enthaltenen Tabellen
};
