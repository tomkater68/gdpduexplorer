/*
    gdpduexplorer tablestructuremodel.h
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "tablestructuremodel.h"
#include "gdpdu.h"
#include "structures.h"

TableStructureModel::TableStructureModel( GDPdU *gdpdu, QObject *parent )
    : QStandardItemModel{parent}
{
    this->gdpdu = gdpdu;

    if ( this->gdpdu == nullptr )
        qFatal( "MediaModel::MediaModel - gdpdu == nullptr" );
}

TableStructureModel::~TableStructureModel()
{
    //Die QStandardItems werden von Qt beseitigt
}

void TableStructureModel::clearModel()
{
    removeRows( 0, rowCount() );
}

void TableStructureModel::updateStructureData( int mediaNo, int tableNo )
{
    clearModel();

    Table* table = gdpdu->getMediaList()->at( mediaNo )->tableList.at( tableNo );

    QStandardItem* itemTable = new QStandardItem(QIcon( ":/icon-table.png" ), table->tableName );
    appendRow( itemTable );

    if ( !table->tableDesc.isEmpty() )
    {
        QStandardItem* itemTableDescription = new QStandardItem(QIcon( ":/icon-text.png" ), table->tableDesc );
        itemTable->appendRow( itemTableDescription );
    } else
    {
        QStandardItem* itemTableDescription = new QStandardItem(QIcon( ":/icon-text.png" ), tr( "Description not available" ) );
        itemTable->appendRow( itemTableDescription );
    }

    QStandardItem* itemTableEntry = new QStandardItem(QIcon( ":/icon-url.png" ), table->url );
    itemTable->appendRow( itemTableEntry );

    if ( !table->dateFormatted.isEmpty() )
    {
        itemTableEntry = new QStandardItem(QIcon( ":/icon-date.png" ), table->dateFormatted );
        itemTable->appendRow( itemTableEntry );
    } else
    {
        itemTableEntry = new QStandardItem(QIcon( ":/icon-text.png" ), tr( "Period not available" ) );
        itemTable->appendRow( itemTableEntry );
    }

    for ( int i = 0; i < table->columnList.size(); i ++ )
    {
        Column* column = table->columnList.at( i );

        QStandardItem* itemColumn = new QStandardItem(QIcon( ":/icon-column.png" ), column->name );
        itemTable->appendRow( itemColumn );

        QString columnDesc = column->desc;
        if ( columnDesc.isEmpty() )
            columnDesc = tr( "No description available" );

        QStandardItem* itemColumnInfo = new QStandardItem(QIcon( ":/icon-text.png" ), columnDesc );
        itemColumn->appendRow( itemColumnInfo );

        if ( column->isPrimaryKey )
        {
            itemColumnInfo = new QStandardItem(QIcon( ":/icon-key.png" ), tr( "Primary key" ) );
            itemColumn->appendRow( itemColumnInfo );
        }

        QString type;
        QIcon icon;

        switch( column->type ) {
        case Column::eAlphanumeric:
            type = tr( "Alphanumeric" );
            icon = QIcon( ":/icon-alphanumeric.png" );
            break;
        case Column::eNumeric:
            type = tr( "Numeric" );
            icon = QIcon( ":/icon-numeric.png" );
            break;
        case Column::eDate:
            type = tr( "Date" );
            icon = QIcon( ":/icon-date.png" );
            break;
        }
        itemColumnInfo = new QStandardItem( icon, type );
        itemColumn->appendRow( itemColumnInfo );

        if ( column->hasMap )
        {
            QStandardItem* itemColumnMap = new QStandardItem(QIcon( ":/icon-map.png" ), tr( "has map" ) );
            itemColumn->appendRow( itemColumnMap );

            for ( auto map : column->mapList )
            {
                itemColumnInfo = new QStandardItem(QIcon( ":/icon-url.png" ), tr( "%1 -> %2" ).arg( map->from ).arg( map->to ) );
                itemColumnMap->appendRow( itemColumnInfo );
            }
        }

        if ( column->isForeignKey )
        {
            QStandardItem* itemColumnForeignKey = new QStandardItem(QIcon( ":/icon-foreign-key.png" ), tr( "Foreign key" ) );
            itemColumn->appendRow( itemColumnForeignKey );

            itemColumnInfo = new QStandardItem(QIcon( ":/icon-url.png" ), tr( "linked to table %1" ).arg( column->foreignKeyReference ) );
            itemColumnForeignKey->appendRow( itemColumnInfo );
        }
    }
}
