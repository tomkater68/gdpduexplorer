/*
    gdpduexplorer mediamodel.h
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QStandardItemModel>

class GDPdU;

/*!\Model für die Anzeige der Medieninhalte eines GDPdU-Satzes in der Treeview
im Hauptfenster */
class MediaModel : public QStandardItemModel
{
    Q_OBJECT
public:
    /*!
        Initialisieren des Objektes
        \param parent Zeiger auf das Elternobjekt
    */
    explicit MediaModel( GDPdU* gdpdu, QObject *parent = nullptr );

    ~MediaModel(); ///<Objekt aufräumen

    /*!
        Gibt die Mediennummer eines im QStandardItem gespeicherten Mediums zurück
        \param item Zeiger auf das QStandardIOtem Objekt
    */
    int getMediaNumberByItem( QStandardItem* item );

    /*!
        Gibt die Tabellennummer einer im QStandardItem gespeicherten Tabelle zurück
        \param item Zeiger auf das QStandardIOtem Objekt
    */
    int getTableNumberByItem( QStandardItem* item );

public slots:
    void updateMediaData(); ///<Model aktualisieren, nachdem neue Daten eingelesen wurden

private:
    GDPdU* gdpdu = nullptr; ///<Zeiger auf das Objekt vom Typ GDPdU

    void clearModel(); ///<Vorhandene Daten im Model löschen

    struct TreeItem
    {
        int media = 0;            ///<Mediennummer, der das Element zugeordnet ist
        int table = -1;           ///<Nummer der Tabelle, der das Element zugeordnet ist
    };

    QMap< QStandardItem*, TreeItem* > itemMap; ///<Mapping-Tabelle für die Auflösung des QStandardItem in das dahinterstehende Medium
};
