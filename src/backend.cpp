/*
    gdpduexplorer appbutton.cpp
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "backend.h"
#include "gdpdu.h"
#include "mediamodel.h"
#include "aboutdialog.h"
#include "csvreader.h"
#include "csvtablemodel.h"
#include "tablestructuremodel.h"

#include <QMessageBox>
#include <QDir>
#include <QSettings>
#include <QFileDialog>

Backend::Backend( Ui::MainWindow *ui, QObject *parent )
    : QObject{parent}
{
    this->ui = ui;
    if ( ui == nullptr )
        qFatal( "Backend::Backend - ui == nullptr" );

    gdpdu = new GDPdU( this );
    if ( gdpdu == nullptr )
        qFatal( "Backend::Backend - gdpdu == nullptr" );

    mediaTreeModel = new MediaModel( getGDPDu(), this );
    if ( mediaTreeModel == nullptr )
        qFatal( "Backend::Backend - mediaTreeModel == nullptr" );

    csvTableModel = new CsvTableModel( this );
    if ( csvTableModel == nullptr )
        qFatal( "Backend::Backend - csvTableModel == nullptr" );

    tableStructureModel = new TableStructureModel( gdpdu, this );
    if ( tableStructureModel == nullptr )
        qFatal( "Backend::Backend - tableStructureModel == nullptr" );

    QObject::connect( ui->appButtonExplore, &AppButton::pressed, this, &Backend::onAppButtonPressed );
    QObject::connect( ui->appButtonSettings, &AppButton::pressed, this, &Backend::onAppButtonPressed );
    QObject::connect( ui->appButtonAbout, &AppButton::pressed, this, &Backend::onAppButtonPressed );
    QObject::connect( ui->appButtonQuit, &AppButton::pressed, this, &Backend::onAppButtonPressed );
    QObject::connect( ui->openIndexButton, &QPushButton::pressed, this, &Backend::onOpenIndexButtonPressed );
    QObject::connect( ui->showTableInfoButton, &QPushButton::pressed, this, &Backend::onShowTableInfoButtonPressed );
    QObject::connect( ui->showTableDataButton, &QPushButton::pressed, this, &Backend::onShowTableDataButtonPressed );
    QObject::connect( this, &Backend::updateCsvProgress, this, &Backend::onUpdateCsvProgress );
    QObject::connect( ui->progressBarImport, &QProgressBar::valueChanged, this, &Backend::onProgressBarImportValueChanged );
    QObject::connect( ui->tableDataBackButton, &QPushButton::pressed, this, &Backend::onTableDataBackButton );
    QObject::connect( ui->tableInformationBackButton, &QPushButton::pressed, this, &Backend::onTableDataBackButton );
    QObject::connect( ui->settingsBackButton, &QPushButton::pressed, this, &Backend::onSettingsBackButton );
    QObject::connect( ui->lineEditSettingsBufferSize, &QLineEdit::textEdited, this, &Backend::onPageSettingsChanged );
    QObject::connect( ui->comboBoxSettingsErrorHandling, &QComboBox::currentIndexChanged, this, &Backend::onPageSettingsChanged );
    QObject::connect( ui->settingsSaveButton, &QPushButton::pressed, this, &Backend::onSettingsSaveButtonPressed );
    QObject::connect( ui->settingsCancelButton, &QPushButton::pressed, this, &Backend::onSettingsCancelButtonPressed );

    ui->treeViewMedia->setModel( getMediaTreeModel() );
    ui->treeViewMedia->setSelectionBehavior( QAbstractItemView::SelectRows );
    ui->treeViewMedia->setEditTriggers( QAbstractItemView::NoEditTriggers );
    ui->tableViewMainTable->setModel( csvTableModel );

    ui->treeViewTableStructure->setSelectionBehavior( QAbstractItemView::SelectRows );
    ui->treeViewTableStructure->setEditTriggers( QAbstractItemView::NoEditTriggers );
    ui->treeViewTableStructure->setModel( tableStructureModel );

    /*set button icons using fontello font */
    ui->appButtonExploreIcon->setText( "\uf0ce" );
    ui->appButtonSettingsIcon->setText( "\ue801" );
    ui->appButtonAboutIcon->setText( "\uf128" );
    ui->appButtonQuitIcon->setText( "\ue802" );

    ui->showTableInfoButton->setVisible( false );
    ui->showTableDataButton->setVisible( false );

    ui->progressBarImport->setVisible( false );
    ui->labelImportStatus->setVisible( false );

    ui->frameExploreButtons->setStyleSheet("#frameExploreButtons { border: 1px solid black; }");
    ui->frameTableDataInfo->setStyleSheet("#frameTableDataInfo { border: 1px solid black; }");
    ui->frameExploreInfo->setStyleSheet("#frameExploreInfo { border: 1px solid black; }");
    ui->treeViewMedia->setStyleSheet("#treeViewMedia { border: 1px solid black; }");
    ui->frameHeaderExplore->setStyleSheet("#frameHeaderExplore { border: 1px solid black; }");
    ui->frameSummary->setStyleSheet("#frameSummary { border: 1px solid black; }");
    ui->frameHeaderTableInformation->setStyleSheet("#frameHeaderTableInformation { border: 1px solid black; }");
    ui->frameTableData->setStyleSheet("#frameTableData { border: 1px solid black; }");
    ui->frameTableDataInfo->setStyleSheet("#frameTableDataInfo { border: 1px solid black; }");
    ui->frameTableDataButtons->setStyleSheet("#frameTableDataButtons { border: 1px solid black; }");
    ui->frameTableDataInformation->setStyleSheet("#frameTableDataInformation { border: 1px solid black; }");
    ui->frameTableInformation->setStyleSheet("#frameTableInformation { border: 1px solid black; }");
    ui->treeViewTableStructure->setStyleSheet("#treeViewTableStructure { border: 1px solid black; }");
    ui->tableViewMainTable->setStyleSheet("#tableViewMainTable { border: 1px solid black; }");
    ui->tableViewLinkedTable->setStyleSheet("#tableViewLinkedTable { border: 1px solid black; }");

    ui->tableViewLinkedTable->setVisible( false );
    ui->settingsCancelButton->setVisible( false );
    ui->settingsSaveButton->setVisible( false);

    ui->lineEditSettingsBufferSize->setText( getParameter( "Import/Buffer", 2048 ).toString() );
    ui->comboBoxSettingsErrorHandling->setCurrentIndex( getParameter( "Import/ErrorHandling", 1 ).toInt() );

    setWidgetStackPage( Pages::ePageExplore );
}

int Backend::yesNoDialog( const QString& message )
{
    QMessageBox msg;
    msg.setText( message );
    msg.setIcon( QMessageBox::Question );
    msg.setStandardButtons( QMessageBox::Yes | QMessageBox::No );
    msg.setStyleSheet( "background-color: white;" );

   int  reply = msg.exec();

    if (reply == QMessageBox::Yes)
        return 1;

    return 0;
}

void Backend::readXMFile( QString& xmlFilePath )
{
    qInfo().nospace() << "Reading XML file " << qPrintable( xmlFilePath );
    gdpdu->readXMLFile( xmlFilePath );
}

GDPdU* Backend::getGDPDu()
{
    return gdpdu;
}

MediaModel* Backend::getMediaTreeModel()
{
    return mediaTreeModel;
}

void Backend::setParameter( const QString& key, const QVariant& value ) const
{
    QSettings settings(  getSettingsPath(), QSettings::IniFormat );
    settings.setValue( key, value );
}

QVariant Backend::getParameter( const QString& key, const QVariant &defaultValue ) const
{
    QSettings settings( getSettingsPath(), QSettings::IniFormat );
    return settings.value( key, defaultValue );
}

void Backend::showMessageDialog( QString title, QString message )
{
    QMessageBox msg;
    msg.setText( message );
    msg.setIcon( QMessageBox::Information );
    msg.setStandardButtons( QMessageBox::Ok );
    msg.setStyleSheet( "background-color: white;" );

    msg.exec();
}

void Backend::setWidgetStackPage( int page )
{
    ui->stackedWidget->setCurrentIndex( page );
}

void Backend::enableMainMenu( bool enable )
{
    ui->appButtonExplore->setEnabled( enable );
    ui->appButtonSettings->setEnabled( enable );
    ui->appButtonAbout->setEnabled( enable );
    ui->appButtonQuit->setEnabled( enable );
    ui->openIndexButton->setEnabled( enable );
    ui->showTableInfoButton->setEnabled( enable );
    ui->showTableDataButton->setEnabled( enable );
    ui->tableDataBackButton->setEnabled( enable );
}

void Backend::onAppButtonPressed()
{
    QObject* obj = sender();

    if ( obj == ui->appButtonExplore )
    {
        if ( ui->stackedWidget->currentIndex() != Pages::ePageExplore )
            setWidgetStackPage( Pages::ePageExplore );
    }
    else if ( obj == ui->appButtonSettings )
    {
        if ( ui->stackedWidget->currentIndex() != Pages::ePageSettings )
            setWidgetStackPage( Pages::ePageSettings );
    }
    else if ( obj == ui->appButtonAbout )
    {
        AboutDialog about;
        about.exec();
    }
    else if ( obj == ui->appButtonQuit ) {
        QApplication::quit();
    }
}

void Backend::onOpenIndexButtonPressed()
{
    //get last selected path and check, if it stills exists
    QString lastXMLPath = getParameter( "Import/LastXMLPath", QString() ).toString();
    QDir lastDir( lastXMLPath );
    if ( !lastDir.exists() )
        lastXMLPath = QString();

    QString fileName = QFileDialog::getOpenFileName( 0, tr( "Open File" ), lastXMLPath, tr( "Xml file (*.xml )" ) );

    if ( !fileName.isEmpty() )
    {
        readXMFile( fileName );

        ui->labelSupplier->setText( getGDPDu()->getSupplier() );
        ui->labelDescription->setText( getGDPDu()->getDescription() );

        ui->labelSummary->setText( tr( "Summary: No. of media: %1 - No. of tables: %2" ).arg( getGDPDu()->getMediaCount() ).arg( getGDPDu()->getTableCount() ) );

        //Save last selected path
        QFileInfo fileInfo( fileName );
        setParameter( "Import/LastXMLPath", fileInfo.absolutePath() );

        ui->treeViewMedia->expandAll();
    }

    if ( getMediaTreeModel()->rowCount() )
    {
        ui->showTableInfoButton->setVisible( true );
        ui->showTableDataButton->setVisible( true );
    }
}

void Backend::onShowTableInfoButtonPressed()
{
    QModelIndexList indexes = ui->treeViewMedia->selectionModel()->selectedIndexes();

    if ( !indexes.count() ) {
        showStatusMessage( tr( "Please select a table" ) );
        return;
    }

    QStandardItem* item = getMediaTreeModel()->itemFromIndex( indexes.at( 0) );
    int tableNumber = getMediaTreeModel()->getTableNumberByItem( item );

    if ( tableNumber < 0 )
    {
        showStatusMessage( tr( "Please select a table" ) );
        return;
    }

    int mediaNumber = getMediaTreeModel()->getMediaNumberByItem( item );

    ui->labelShowTableInformationMediaInformation->setText( gdpdu->getMediaList()->at( mediaNumber )->name );
    ui->labelShowTableInformationTableInformation->setText( gdpdu->getMediaList()->at( mediaNumber )->tableList.at( tableNumber )->tableName );

    tableStructureModel->updateStructureData( mediaNumber, tableNumber );
    ui->treeViewTableStructure->expandAll();
    setWidgetStackPage( ePageTableInformation );

}

void Backend::onShowTableDataButtonPressed()
{
    QModelIndexList indexes = ui->treeViewMedia->selectionModel()->selectedIndexes();

    if ( !indexes.count() ) {
        showStatusMessage( tr( "Please select a table" ) );
        return;
    }

    QStandardItem* item = getMediaTreeModel()->itemFromIndex( indexes.at( 0) );
    int tableNumber = getMediaTreeModel()->getTableNumberByItem( item );

    if ( tableNumber < 0 )
    {
        showStatusMessage( tr( "Please select a table" ) );
        return;
    }

    int mediaNumber = getMediaTreeModel()->getMediaNumberByItem( item );

    ui->labelShowTableDataMediaInformation->setText( gdpdu->getMediaList()->at( mediaNumber )->name );
    ui->labelShowTableDataTableInformation->setText( gdpdu->getMediaList()->at( mediaNumber )->tableList.at( tableNumber )->tableName );
    setWidgetStackPage( ePageTableData );

    bool success = readCSVFile( mediaNumber, tableNumber );

    if ( success )
        csvTableModel->setTable( gdpdu->getMediaList()->at( mediaNumber )->tableList.at( tableNumber ) );
}

void Backend::onUpdateCsvProgress( int progress )
{
    ui->progressBarImport->setValue( progress );
}

void Backend::onProgressBarImportValueChanged( int progress )
{
    if ( progress > 0 && progress < 100 )
    {
        ui->progressBarImport->setVisible( true );
        ui->labelImportStatus->setVisible( true );
    } else
    {
        ui->progressBarImport->setVisible( false );
        ui->labelImportStatus->setVisible( false);
    }
}

void Backend::onTableDataBackButton()
{
    setWidgetStackPage( ePageExplore );
}

void Backend::onSettingsBackButton()
{
    if ( pageSettingsChanged )
    {
        if ( !yesNoDialog( tr( "Do you want to discard the changes?" ) ) )
        {
            onSettingsCancelButtonPressed();
            return;
        }
    }

    setWidgetStackPage( ePageExplore );

}

void Backend::onPageSettingsChanged()
{
    pageSettingsChanged = true;
    ui->settingsCancelButton->setVisible( true );
    ui->settingsSaveButton->setVisible( true);
}

void Backend::onSettingsSaveButtonPressed()
{
    setParameter( "Import/Buffer", ui->lineEditSettingsBufferSize->text() );
    setParameter( "Import/ErrorHandling", ui->comboBoxSettingsErrorHandling->currentIndex() );
    ui->settingsCancelButton->setVisible( false );
    ui->settingsSaveButton->setVisible( false);
    pageSettingsChanged = false;
}

void Backend::onSettingsCancelButtonPressed()
{
    ui->lineEditSettingsBufferSize->setText( getParameter( "Import/Buffer", 2048 ).toString() );
    ui->comboBoxSettingsErrorHandling->setCurrentIndex( getParameter( "Import/ErrorHandling", 1 ).toInt() );

    pageSettingsChanged = false;
    ui->settingsCancelButton->setVisible( false );
    ui->settingsSaveButton->setVisible( false);
}

QString Backend::getSettingsPath() const
{
    return QString( "%1/.gdpduexplorer/settings.ini" ).arg( QDir::homePath() );
}

bool Backend::readCSVFile( const int mediaNo, const int tableNo ) {

    if ( gdpdu == nullptr )
        qFatal( "Backend::readCSVFile - GDPdU == nullptr" );

    CsvReader csvreader( this, gdpdu );

    return csvreader.readCSV( mediaNo, tableNo );
}
