#include "aboutdialog.h"
#include "ui_aboutdialog.h"

#include <QDate>

AboutDialog::AboutDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::AboutDialog)
{
    ui->setupUi(this);

    QObject::connect( ui->buttonClose, &QPushButton::pressed, this, &AboutDialog::close );

    ui->labelVersion->setText( tr( "Version " ) + QApplication::applicationVersion() );

    QDate buildDate = QDate::fromString(QString(__DATE__).simplified(), "MMM d yyyy");
    QTime buildTime = QTime::fromString(QString(__TIME__).simplified(), "hh:mm:ss");

    ui->labelBuild->setText( QString( "Build %1.%2" ).arg( buildDate.toString( "yyyyMMdd" ) , buildTime.toString( "hhmmss" ) ) );
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
