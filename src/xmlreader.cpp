/*
    gdpduexplorer xmlreaxder.cpp
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "xmlreader.h"
#include "gdpdu.h"
#include "structures.h"

#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QXmlStreamReader>

XmlReader::XmlReader( GDPdU *gdpdu, Backend *backend, QObject *parent )
    : QObject{parent} {

    this->backend = backend;
    this->gdpdu = gdpdu;
}

bool XmlReader::readXml(QString& fileName) {

    QFile file( fileName );

    QXmlStreamReader reader;

    if( !file.open(QFile::ReadOnly | QFile::Text) ){

        errorMessage = tr( "Unable to open file %1." ).arg( fileName );
        qWarning().nospace() << "Unable to load file " << qPrintable( fileName );
        return false;
    }

    QFileInfo fileInfo( file.fileName() );
    workingPath = fileInfo.path();

    reader.setDevice( &file );

    if ( reader.readNextStartElement() ) {
        if (reader.name().toString() == "DataSet")
            readDataSet( &reader );
        else
            reader.raiseError( tr( "XML-File invalid (Missing <DataSet>-Tag)" ) );
    }

    if ( reader.hasError() ) {
        errorMessage = reader.errorString();

        //Informationen über den Datenlieferanten zurücksetzen, wenn diese bereits gesetzt sind
        gdpdu->setSupplierName( QString() );
        gdpdu->setLocation( QString() );
        gdpdu->setDescription( QString() );
    }

    return !reader.error();
}

QString XmlReader::getLastError() {

    return errorMessage;
}

void XmlReader::readDataSet( QXmlStreamReader* reader ) {

    while( reader->readNextStartElement() ){
        if(reader->name().toString() == "DataSupplier")
            readDataSupplier( reader );
        else if(reader->name().toString() == "Media")
            readMedia( reader);
        else
            reader->skipCurrentElement();
    }
}

void XmlReader::readDataSupplier( QXmlStreamReader* reader ) {

    QString supplierName;
    QString location;
    QString comment;

    while( reader->readNextStartElement() ){

        QString elementName = reader->name().toString();

        if( elementName == "Name" ){
            supplierName = reader->readElementText();
            gdpdu->setSupplierName( supplierName );
        }
        else if ( elementName == "Location" ) {
            location = supplierName = reader->readElementText();
            gdpdu->setLocation( location );
        }
        else if ( elementName == "Comment" ) {
            comment = reader->readElementText();
            gdpdu->setDescription( comment );
        } else
           reader->skipCurrentElement();
    }
}

void XmlReader::readMedia( QXmlStreamReader* reader ) {

    Media* media = gdpdu->addMedia();
    media->mediaNo = mediaNumber;
    mediaNumber++;

    tableNumber = 0;

    while( reader->readNextStartElement() ){

        QString elementName = reader->name().toString();

        if( elementName == "Name" )
            media->name = reader->readElementText();
        else if ( elementName == "Table" )
            readTable( reader, media );
        else
            reader->skipCurrentElement();
    }
}

void XmlReader::readTable( QXmlStreamReader* reader, Media* media ) {

    media->tableList.push_back( new Table );
    Table* table = media->tableList.back();

    table->tableNo = tableNumber;
    tableNumber++;

    while( reader->readNextStartElement() ){

        QString elementName = reader->name().toString();

        if( elementName == "URL" ) {
            QString url = reader->readElementText();
            table->url = url;

            QDir dir;
            dir.setCurrent( workingPath );
            table->urlFullPath = dir.absoluteFilePath( url );
        }
        else if( elementName == "Name" )
            table->tableName = reader->readElementText();
        else if( elementName == "Description" )
            table->tableDesc = reader->readElementText();
        else if( elementName == "ANSI" ) {
            table->codec = Table::eANSI;
            reader->skipCurrentElement();
        }
        else if( elementName == "Macintosh" ) {
            table->codec = Table::eMacintosh;
            reader->skipCurrentElement();
        }
        else if( elementName == "OEM" ){
            table->codec = Table::eOEM;
            reader->skipCurrentElement();
        }
        else if( elementName == "UTF16" ) {
            table->codec = Table::eUTF16;
            reader->skipCurrentElement();
        }
        else if( elementName == "UTF8" ) {
            table->codec = Table::eUTF8;
            reader->skipCurrentElement();
        }
        else if( elementName == "UTF7" ) {
            table->codec = Table::eUTF7;
            reader->skipCurrentElement();
        }
        else if( elementName == "DecimalSymbol" )
            table->decimalSymbol = reader->readElementText();
        else if( elementName == "DigitGroupingSymbol" )
            table->digitGrouping = reader->readElementText();
        else if( elementName == "SkipNumBytes" )
            table->skipBytes = reader->readElementText().toInt();
        else if( elementName == "Epoch" )
            table->epoch = reader->readElementText().toInt();
        else if( elementName == "Range" )
            readTableRange( reader, table );
        else if( elementName == "Validity" )
            readValidity( reader, table );
        else if( elementName == "VariableLength" )
            readVariableLength( reader, table );
        else if( elementName == "FixedLength" )
            readFixedLength( reader, table );
        else
            reader->skipCurrentElement();
    }

    formatDate( table );
}

void XmlReader::readTableRange( QXmlStreamReader* reader, Table* table ) {

    while( reader->readNextStartElement() ){

        QString elementName = reader->name().toString();

        if( elementName == "From" )
            table->rangeFrom = reader->readElementText().toInt();
        else if( elementName == "To" )
            table->rangeTo = reader->readElementText().toInt();
        else
            reader->skipCurrentElement();
    }
}

void XmlReader::readValidity( QXmlStreamReader* reader, Table* table ) {

    while( reader->readNextStartElement() ){

        QString elementName = reader->name().toString();

        if( elementName == "Range" )
            readValidityRange( reader, table);
        else if( elementName == "Format" ) {
            //make date format compatible to Qt
            table->dateFormat = reader->readElementText();
            table->dateFormat = table->dateFormat.toLower();
            table->dateFormat = table->dateFormat.replace( "m", "M" );
        }
        else
            reader->skipCurrentElement();
    }
}

void XmlReader::readValidityRange( QXmlStreamReader* reader, Table* table ) {

    while( reader->readNextStartElement() ){

        QString elementName = reader->name().toString();

        if( elementName == "From" )
            table->from = reader->readElementText();
        else if( elementName == "To" )
            table->to = reader->readElementText();
        else
            reader->skipCurrentElement();
    }
}

void XmlReader::readVariableLength( QXmlStreamReader* reader, Table* table ) {

    while( reader->readNextStartElement() ){

        QString elementName = reader->name().toString();

        if( elementName == "VariablePrimaryKey" )
            readVariableColumn( reader, table, true );
        else if( elementName == "VariableColumn" )
            readVariableColumn( reader, table, false );
        else if( elementName == "ForeignKey" )
            readForeignKey( reader, table );
        else if( elementName == "ColumnDelimiter" )
            table->columnDelimiter = reader->readElementText();
        else if( elementName == "RecordDelimiter" )
            table->recordDelimiter = reader->readElementText();
        else if( elementName == "TextEncapsulator" )
            table->textEncapsulator = reader->readElementText();
        else
            reader->skipCurrentElement();
    }
}

void XmlReader::readFixedLength( QXmlStreamReader* reader, Table* table ) {

    table->isFixedLength = true;

    while( reader->readNextStartElement() ){

        QString elementName = reader->name().toString();

        if( elementName == "FixedPrimaryKey" )
            readVariableColumn( reader, table, true );
        else if( elementName == "FixedColumn" )
            readVariableColumn( reader, table, false );
        else if( elementName == "ForeignKey" )
            readForeignKey( reader, table );
        else if( elementName == "Length" )
            table->fixedLength = reader->readElementText().toInt();
        else if( elementName == "RecordDelimiter" )
            table->recordDelimiter = reader->readElementText();
        else
            reader->skipCurrentElement();
    }
}

void XmlReader::readForeignKey( QXmlStreamReader* reader, Table* table ) {

    QString reference;
    QVector<QString> nameList;

    while( reader->readNextStartElement() ){

        QString elementName = reader->name().toString();

        if( elementName == "Name" )
            nameList.push_back( reader->readElementText() );
        else if( elementName == "Reference" )
            reference = reader->readElementText();
        else
            reader->skipCurrentElement();
    }

    for ( const QString& name: std::as_const( nameList ) )
    {
        for ( auto field : table->columnList )
        {
            if ( field->name == name )
            {
                field->isForeignKey = true;
                field->foreignKeyReference = reference;
            }
        }
    }
}

void XmlReader::readVariableColumn(QXmlStreamReader* reader, Table* table, bool isPrimaryKey ) {

    table->columnList.push_back( new Column );
    Column* column = table->columnList.back();

    column->dateFormat = table->dateFormat;
    column->epoch = table->epoch;

    if ( isPrimaryKey )
        column->isPrimaryKey = true;

    while( reader->readNextStartElement() ){

        QString elementName = reader->name().toString();

        if( elementName == "Name" )
            column->name = reader->readElementText();
        else if( elementName == "Description" )
            column->desc = reader->readElementText();
        else if( elementName == "AlphaNumeric" ) {
            column->type = Column::eAlphanumeric;
            reader->skipCurrentElement();
        }
        else if( elementName == "Numeric" )
            readAccuracy( reader, column );
        else if( elementName == "Date" ) {
            column->type = Column::eDate;
            reader->skipCurrentElement();
        }
        else if( elementName == "MaxLength" )
            column->maxLength = reader->readElementText().toInt();
        else if( elementName == "Map" )
            readMap( reader, column );
        else
            reader->skipCurrentElement();
    }
}

void XmlReader::readFixedColumn(QXmlStreamReader* reader, Table* table, bool isPrimaryKey ) {

    table->columnList.push_back( new Column );
    Column* column = table->columnList.back();

    column->dateFormat = table->dateFormat;
    column->epoch = table->epoch;

    if ( isPrimaryKey )
        column->isPrimaryKey = true;

    while( reader->readNextStartElement() ){

        QString elementName = reader->name().toString();

        if( elementName == "Name" )
            column->name = reader->readElementText();
        else if( elementName == "Description" )
            column->desc = reader->readElementText();
        else if( elementName == "AlphaNumeric" )
            column->type = Column::eAlphanumeric;
        else if( elementName == "Numeric" )
            readAccuracy( reader, column );
        else if( elementName == "Date" )
            column->type = Column::eDate;
        else if( elementName == "Map" )
            readMap( reader, column );
        else if( elementName == "FixedRange" )
            readFixedRange( reader, column );
        else
            reader->skipCurrentElement();
    }
}

void XmlReader::readAccuracy( QXmlStreamReader* reader, Column* column ) {

    column->type = Column::eNumeric;

    while( reader->readNextStartElement() ){

        QString elementName = reader->name().toString();

        if( elementName == "Accuracy" )
            column->accuracy = reader->readElementText().toInt();
        else if( elementName == "ImpliedAccuracy" )
            column->impliedAccuracy = reader->readElementText().toInt();
        else
            reader->skipCurrentElement();
    }
}

void XmlReader::readMap( QXmlStreamReader* reader, Column* column ) {

    column->mapList.push_back( new Map );
    Map* map = column->mapList.back();

    while( reader->readNextStartElement() ){

        QString elementName = reader->name().toString();

        if( elementName == "From" ) {
            map->from = reader->readElementText();
            column->hasMap = true;
        }
        else if( elementName == "To" )
            map->to = reader->readElementText();
        else
            reader->skipCurrentElement();
    }
}

void XmlReader::readFixedRange( QXmlStreamReader* reader, Column* column ) {

    while( reader->readNextStartElement() ){

        QString elementName = reader->name().toString();

        if( elementName == "From" )
            column->fixedFrom = reader->readElementText().toInt();
        else if( elementName == "To" )
            column->fixedTo = reader->readElementText().toInt();
        else if( elementName == "Length" )
            column->fixedLength = reader->readElementText().toInt();
        else
            reader->skipCurrentElement();
    }
}

void XmlReader::formatDate( Table* table ) {

    QDate from;
    QDate to;

    if ( table->from.isEmpty() && table->to.isEmpty() ) {
        table->dateFormatted = tr( "not defined");
        return;
    }

    if ( table->dateFormat.contains( "yyyy" ) )
    {
        from = QDate::fromString( table->from, table->dateFormat );
        to = QDate::fromString( table->to, table->dateFormat );
    } else
    {
        int posYear = table->dateFormat.indexOf( "y" );
        int posMonth = table->dateFormat.indexOf( "M" );
        int posDay = table->dateFormat.indexOf( "d" );

        int year = table->from.mid( posYear, 2 ).toInt();
        if ( year < table->epoch )
            year += 2000;
        else
            year += 1900;

        from = QDate( year, table->from.mid( posMonth, 2 ).toInt(), table->from.mid( posDay, 2 ).toInt() );

        year = table->to.mid( posYear, 2 ).toInt();
        if ( year < table->epoch )
            year += 2000;
        else
            year += 1900;

        to = QDate( year, table->to.mid( posMonth, 2 ).toInt(), table->from.mid( posDay, 2 ).toInt() );
    }

    if ( from.isValid() && to.isValid() )
        table->dateFormatted =  QString( "%1 - %2" ).arg( from.toString( "dd.MM.yyyy" ) )
                                   .arg( to.toString( "dd.MM.yyyy" ) );
    else if ( from.isValid() && !to.isValid() )
        table->dateFormatted =  QString( "ab %1" ).arg( from.toString( "dd.MM.yyyy" ) );
    else
        table->dateFormatted = QString();
}
