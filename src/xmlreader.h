/*
    gdpduexplorer xmlreader.h
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <QObject>

class Backend;
class GDPdU;
class QXmlStreamReader;
class Media;
class Table;
class Column;

/*!\brief Lesen und Parsen der GDPdU-Beschreibungsdatei (index.xml) */
class XmlReader : public QObject
{
    Q_OBJECT
public:
    /*!
        Objekt initialisieren
        \param gdpdu Zeiger auf das GDPdU Objekt
        \param backend pointer to Backend Objekt
        \param parent Zeiger auf das Elternobjekt
    */
    explicit XmlReader(GDPdU* gdpdu, Backend* backend, QObject *parent = nullptr);

    /*!
        Index.xml lesen
        \param fileName Pfad zur XML-Datei
        \return true, wenn die Datei erfolgreich gelesen wurde, sonst false
    */
    bool readXml( QString& fileName );

    QString getLastError(); ///<letzten Fehler beim Lesen/Parsen zurückgeben

private:
    Backend* backend = nullptr; ///<Zeiger auf das Backend Objekt
    GDPdU* gdpdu = nullptr; ///<Zeiger auf das GSPdU-Objekt;
    QXmlStreamReader* reader = nullptr; ///<Zeiger auf das QXmlStreamReader Objekt

    QString workingPath; ///<XML Verzeichnis. Wird zur Auflösung relativer Pfade benötigt
    QString errorMessage; ///<Letzte Fehlermeldung beim Lesen/Parsen der Beschreibungsdatei
    int mediaNumber = 0;  ///<Anzahl der im Datensatz enthaltenen Medien
    int tableNumber = 0;  ///<Anzahl der im Datensatz enthaltenen Tabellen

    void readDataSet( QXmlStreamReader* reader ); ///< Abschnitt <DataSet> lesen
    void readDataSupplier( QXmlStreamReader* reader ); ///<Abschnitt <DataSupplier> lesem
    void readMedia( QXmlStreamReader* reader ); ///<Abschnitt <Media> lesen
    void readTable( QXmlStreamReader* reader, Media* media ); ///<Abschnitt <Table> lesen
    void readTableRange( QXmlStreamReader* reader, Table* table ); ///<Abschnitt <Range> lesen
    void readValidity( QXmlStreamReader* reader, Table* table ); ///<Abschnitt <Validity> lesen
    void readValidityRange( QXmlStreamReader* reader, Table* table ); ///<Abschnitt <ValidityRange> lesne
    void readVariableLength( QXmlStreamReader* reader, Table* table ); ///<Abschnitt <VariableLength> lesen
    void readFixedLength( QXmlStreamReader* reader, Table* table ); ///<Abschnitt <FixedLength> lesen
    void readVariableColumn( QXmlStreamReader* reader, Table* table, bool isPrimaryKey ); ///<Abscnitt <VariableColumn> lesen
    void readFixedColumn( QXmlStreamReader* reader, Table* table, bool isPrimaryKey ); ///<Abschnitt <FixedColumn> lesen
    void readForeignKey( QXmlStreamReader* reader, Table* table ); ///<Abschnitt <ForeignKey> lesen
    void readAccuracy( QXmlStreamReader* reader, Column* column ); ///<Abschnitt <Accuracy> lesen
    void readMap( QXmlStreamReader* reader, Column* column ); ///<Abschnitt <Map> lesen
    void readFixedRange( QXmlStreamReader* reader, Column* column ); ///<Abschnitt <FixedRange> lesen

    void formatDate( Table* table ); ///<Datumsfelder in der Tabellembeschreibung entsprechend des Datumsformats formatieren
};
