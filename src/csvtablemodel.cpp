/*
    gdpduexplorer csvtablemodel.cpp
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "csvtablemodel.h"

#include <QColor>
#include <QIcon>

CsvTableModel::CsvTableModel( QObject *parent )
    : QAbstractTableModel{parent} {

}

CsvTableModel::~CsvTableModel() {

    setTable( nullptr );
}

int CsvTableModel::rowCount( const QModelIndex& parent ) const {

    Q_UNUSED( parent );

    if ( table == nullptr )
        return 0;
    else
        return table->columnValueList.size();
}

int CsvTableModel::columnCount( const QModelIndex& parent ) const {

    Q_UNUSED( parent );

    if ( table == nullptr )
        return 0;
    else
        return table->columnList.size();
}

QVariant CsvTableModel::data( const QModelIndex& index, int role ) const {

    switch(role){
    case Qt::DisplayRole:
        return table->columnValueList.at( index.row() ).at( index.column() );
        break;

    case Qt::BackgroundRole:
        if ( table->columnList.at( index.column() )->isForeignKey )
            return QColor( 234, 228, 228 );

        if ( table->columnList.at( index.column() )->isPrimaryKey )
            return QColor( 253, 214, 214 );
        break;

     case Qt::DecorationRole:
        if ( table->columnList.at( index.column() )->isPrimaryKey )
            return QIcon( "/icon-key.png" );
        break;
    }

    return QVariant();
}

QVariant CsvTableModel::headerData( int section, Qt::Orientation orientation, int role ) const {

    if ( table == nullptr )
        return QVariant();

    if ( role != Qt::DisplayRole )
        return QVariant();

    if ( role == Qt::DisplayRole && orientation == Qt::Horizontal )
        return table->columnList.at( section )->name;

    return QVariant();
}

void CsvTableModel::setTable( Table *table ) {

    beginResetModel();
    this->table = table;
    endResetModel();
}
