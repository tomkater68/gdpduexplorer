<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../ui/aboutdialog.ui" line="26"/>
        <source>About gdpduexplorer</source>
        <translation>Über gdpduexplorer</translation>
    </message>
    <message>
        <location filename="../../ui/aboutdialog.ui" line="97"/>
        <source>Import and view GDPdU data</source>
        <translation>GDPdU Daten importieren und visualisieren</translation>
    </message>
    <message>
        <location filename="../../ui/aboutdialog.ui" line="233"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../../aboutdialog.cpp" line="14"/>
        <source>Version </source>
        <translation>Version </translation>
    </message>
</context>
<context>
    <name>Backend</name>
    <message>
        <location filename="../../backend.cpp" line="223"/>
        <source>Open File</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location filename="../../backend.cpp" line="223"/>
        <source>Xml file (*.xml )</source>
        <translation>Xml Datei (*.xml )</translation>
    </message>
    <message>
        <location filename="../../backend.cpp" line="232"/>
        <source>Summary: No. of media: %1 - No. of tables: %2</source>
        <translation>Zusammenfassung: Anzahl Medien: %1 - Anzahl Tabellen: %2</translation>
    </message>
    <message>
        <location filename="../../backend.cpp" line="253"/>
        <location filename="../../backend.cpp" line="262"/>
        <location filename="../../backend.cpp" line="282"/>
        <location filename="../../backend.cpp" line="291"/>
        <source>Please select a table</source>
        <translation>Bitte eine Tabelle auswählen</translation>
    </message>
    <message>
        <location filename="../../backend.cpp" line="334"/>
        <source>Do you want to discard the changes?</source>
        <translation>Sollen die Änderungen verworfen werden?</translation>
    </message>
</context>
<context>
    <name>CsvReader</name>
    <message>
        <location filename="../../csvreader.cpp" line="66"/>
        <location filename="../../csvreader.cpp" line="73"/>
        <location filename="../../csvreader.cpp" line="80"/>
        <source>Error reading csv file</source>
        <translation>Fehler beim Lesen der CSV-Datei</translation>
    </message>
    <message>
        <location filename="../../csvreader.cpp" line="66"/>
        <source>File %1 not found!</source>
        <translation>Datei %1 nicht gefunden!</translation>
    </message>
    <message>
        <location filename="../../csvreader.cpp" line="73"/>
        <source>Unable to open file %1!</source>
        <translation>Datei %1 kann nicht geöffnet werden!</translation>
    </message>
    <message>
        <location filename="../../csvreader.cpp" line="80"/>
        <source>Nicht unterstützte Zeichensatzkodierung.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../csvreader.cpp" line="169"/>
        <source>Import finished with errors</source>
        <translation>Der Import endete mit Fehlern</translation>
    </message>
    <message>
        <location filename="../../csvreader.cpp" line="169"/>
        <source>CSV import finished with errors. %1 records rejected</source>
        <translation>Der CSV-Import wurde mit Fehlern beendet. %1 Datensätze wurden verworfen</translation>
    </message>
</context>
<context>
    <name>GDPdU</name>
    <message>
        <location filename="../../gdpdu.cpp" line="49"/>
        <source>Open a valid index.xml file</source>
        <translation>Bitte eine gültige index.xml Datei öffnen</translation>
    </message>
    <message>
        <location filename="../../gdpdu.cpp" line="85"/>
        <source>Error reading index.xml</source>
        <translation>Fehler beim Lesen der index.xml</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../ui/mainwindow.ui" line="25"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="147"/>
        <location filename="../../mainwindow.cpp" line="61"/>
        <source>Explore</source>
        <translation>Visualisieren</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="224"/>
        <location filename="../../mainwindow.cpp" line="65"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="301"/>
        <source>App info</source>
        <translation>Über das Programm</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="378"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="629"/>
        <source>No data available. Open index.xml.</source>
        <translation>Keine Daten vorhanden. Bitte eine Beschreibungsdatei (index.xml) Datei öffnen</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="455"/>
        <source>Open index.xml</source>
        <translation>Index.xml öffnen</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="486"/>
        <source>Show table info</source>
        <translation>Tabelleninfo anzeigen</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="517"/>
        <source>Show table data</source>
        <translation>Tabellendaten anzeigen</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="774"/>
        <location filename="../../ui/mainwindow.ui" line="1015"/>
        <location filename="../../ui/mainwindow.ui" line="1316"/>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="805"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="836"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="862"/>
        <source>CSV Import</source>
        <translation>CSV Import</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="874"/>
        <source>Buffer size:</source>
        <translation>Pufffergröße:</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="913"/>
        <source>Error handling:</source>
        <translation>Fehlerbehandlung:</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="924"/>
        <source>Ignore row</source>
        <translation>Zeile ignorieren</translation>
    </message>
    <message>
        <source>Ignore error</source>
        <translation type="vanished">Fehler ignorieren</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="929"/>
        <source>Cancel import</source>
        <translation>Import abbrechen</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="1040"/>
        <source>Verknüpfte Daten</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="1107"/>
        <source>Importing table data</source>
        <translation>Importiere Tabellendaten</translation>
    </message>
    <message>
        <location filename="../../ui/mainwindow.ui" line="1207"/>
        <location filename="../../ui/mainwindow.ui" line="1417"/>
        <source>Media information</source>
        <translation>Information über das Medium</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="34"/>
        <source>gdpduexplorer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="69"/>
        <source>View table data</source>
        <translation>Ansicht Tabellendaten</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="73"/>
        <source>View table structure</source>
        <translation>Tabellenstruktur anzeigen</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="82"/>
        <source>Do you really want to quit the application?</source>
        <translation>Soll die Anwendung wirklich beendet werden?</translation>
    </message>
</context>
<context>
    <name>TableStructureModel</name>
    <message>
        <location filename="../../tablestructuremodel.cpp" line="56"/>
        <source>Description not available</source>
        <translation>Keine Beschreibung verfügbar</translation>
    </message>
    <message>
        <location filename="../../tablestructuremodel.cpp" line="69"/>
        <source>Period not available</source>
        <translation>Periode nicht verfügbar</translation>
    </message>
    <message>
        <location filename="../../tablestructuremodel.cpp" line="82"/>
        <source>No description available</source>
        <translation>Keine Beschreibung verfügbar</translation>
    </message>
    <message>
        <location filename="../../tablestructuremodel.cpp" line="89"/>
        <source>Primary key</source>
        <translation>Primärschlüssel</translation>
    </message>
    <message>
        <location filename="../../tablestructuremodel.cpp" line="98"/>
        <source>Alphanumeric</source>
        <translation>Alphanumerisch</translation>
    </message>
    <message>
        <location filename="../../tablestructuremodel.cpp" line="102"/>
        <source>Numeric</source>
        <translation>Numerisch</translation>
    </message>
    <message>
        <location filename="../../tablestructuremodel.cpp" line="106"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../../tablestructuremodel.cpp" line="115"/>
        <source>has map</source>
        <translation>Wertetabelle</translation>
    </message>
    <message>
        <location filename="../../tablestructuremodel.cpp" line="120"/>
        <source>%1 -&gt; %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../tablestructuremodel.cpp" line="127"/>
        <source>Foreign key</source>
        <translation>Fremdschlüssel</translation>
    </message>
    <message>
        <location filename="../../tablestructuremodel.cpp" line="130"/>
        <source>linked to table %1</source>
        <translation>verknüpft mit Tabelle %1</translation>
    </message>
</context>
<context>
    <name>XmlReader</name>
    <message>
        <location filename="../../xmlreader.cpp" line="43"/>
        <source>Unable to open file %1.</source>
        <translation>Datei %1 kann nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../../xmlreader.cpp" line="57"/>
        <source>XML-File invalid (Missing &lt;DataSet&gt;-Tag)</source>
        <translation>Ungültige XML-Datei (&lt;DataSet&gt;- nicht vorhanden)</translation>
    </message>
    <message>
        <location filename="../../xmlreader.cpp" line="464"/>
        <source>not defined</source>
        <translation>Nicht definiert</translation>
    </message>
</context>
</TS>
