/*
    gdpduexplorer gdpdu.cpp
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gdpdu.h"
#include "xmlreader.h"
#include "backend.h"

#include <QUrl>
#include <QDir>

GDPdU::GDPdU( Backend *backend )
    : QObject{backend}
{
    this->backend = backend;

    if ( backend == nullptr )
        qFatal( "GDPdU::GDPdU - Backend == nullptr" );

    reader = new XmlReader( this, backend, this );

    if ( !reader )
        qFatal( "GDPdU::GDPdU - XmlReader == nullptr" );
}

GDPdU::~GDPdU()
{

    clearMediaList();
}

QString GDPdU::getSupplier()
{
    if ( supplier.isEmpty() )
        return tr( "Open a valid index.xml file" );

    if ( location.isEmpty() )
        return supplier;

    return QString( "%1, %2" ).arg( supplier, location );
}

QString GDPdU::getDescription()
{
    return description;
}

int GDPdU::getMediaCount()
{
    return mediaList.size();
}

int GDPdU::getTableCount()
{
    int numTables = 0;

    for ( auto media : mediaList )
        numTables += media->tableList.size();

    return numTables;
}

void GDPdU::readXMLFile( QString &fileName )
{
    //remove previously loaded media data
    clearMediaList();

    XmlReader reader( this, backend );

    if ( !reader.readXml( fileName ) ) {
        emit backend->showMessageDialog( tr( "Error reading index.xml" ),reader.getLastError() );
        qWarning().nospace() << "Error reading index.xml " << qPrintable( reader.getLastError()  );
    }

    emit mediaListUpdated();
}

Media* GDPdU::addMedia()
{
    mediaList.append( new Media );
    return mediaList.back();
}

void GDPdU::setSupplierName( const QString& name )
{
    supplier = name;
}

void GDPdU::setLocation( const QString& location )
{
    this->location = location;
}

void GDPdU::setDescription( const QString& description )
{
    this->description = description;
}

QVector<Media*>* GDPdU::getMediaList()
{
    return &mediaList;
}

void GDPdU::clearMediaList()
{
    while ( !mediaList.isEmpty() ) {
        Media* media = mediaList.takeFirst();

        while ( !media->tableList.isEmpty() ) {
            Table* table = media->tableList.takeFirst();

            while ( !table->columnList.isEmpty() ) {
                Column *column = table->columnList.takeFirst();

                if ( column->hasMap ) {
                    while ( !column->mapList.isEmpty() ) {
                        Map* map = column->mapList.takeFirst();
                        delete map;
                    }
                }
                delete column;
            }

            if ( table->columnValueList.size() ) {
                for ( int i = 0; i < table->columnValueList.size(); i++ ) {
                    QVector<QString> column = table->columnValueList.at( i );
                    column.clear();
                }
                table->columnValueList.clear();

            }
            delete table;
        }
        delete media;
    }

    emit mediaListCleared();
}
