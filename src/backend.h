/*
    gdpduexplorer backend.h
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "./ui_mainwindow.h"
#include <QObject>

class GDPdU;
class MediaModel;
class CsvTableModel;
class TableStructureModel;

enum Pages {
    ePageExplore,   ///<Seite 'Daten visualisieren'
    ePageSettings,  ///<Seite "Einstellungen"
    ePageTableData,  ///<Seite "Tabellendaten"
    ePageTableInformation ///<Seite "Tabellenstruktur"
};

/*!\brief Im Backend werden globale, von allen Programmteilen verwendete Funktionen
    realisiert */
class Backend : public QObject
{
    Q_OBJECT

public:
    /*!
        Backend initialisieren
        \param ui Zeiger auf die Ui::MainWindow mit den GUI-Elementen
        \param parent Zeiger auf das Elternobjekt
    */
    explicit Backend( Ui::MainWindow *ui, QObject *parent = nullptr );

    /*!
        Anzeige eines Ja/Nein Dialogs
        \param message Nachricht, die im Dialog angezeigt werden soll
        \return 0 (Nein) or 1 (Ja)
    */
    int yesNoDialog( const QString& message );

    /*!
        XML-Datei lesen
        \param xmlFilePath Pfad und Dateiname der zu lesenden Datei
    */
    void readXMFile( QString& xmlFilePath );

    /*!
        Zeiger auf das GDPdU Objekt holen
        \return Zeiger auf das GDPdU Objekt
    */
    GDPdU* getGDPDu();

    /*!
        Zeiger auf das MediaModel Objekt holen
        \return Zeiger auf das MediaModel Objekt
    */
    MediaModel* getMediaTreeModel();

    /*!
        Setzen eines Parameters
        \param key Schlüssel des zu setzenden Parameters
        \param value Zu setzender Wert des Parameters
    */
    void setParameter( const QString& key, const QVariant& value ) const;

    /*!
        Wert eines Parameters holen
        \param key Schlüssel des zu holenden Parameters
        \param defaultValue Standardwert, wenn der Parameter nicht vorhanden ist
    */
    QVariant getParameter( const QString& key, const QVariant &defaultValue ) const;

    /*!
        Nachrichtendialog anzeigen
        \param title Titel der Nachricht
        \param message Anzuzeigende Nachricht
    */
    void showMessageDialog( QString title, QString message );

    /*!
        Neue Seite im WidgetStack anzeigen
        \param page Index der anzuzeigenden Seite
    */
    void setWidgetStackPage( int page );

    /*!
        Menüschalter für den Import von CSV-Dateien aktivieren/deaktivieren
        \param enable true, wenn die Schalter aktiviert werden sollen, sonst false
    */
    void enableMainMenu( bool enable ); ///<Menüschalter für den Import von CSV-Dateien aktivieren/deaktivieren

signals:
    void showStatusMessage( const QString& message ); ///<Meldung in der Statuszeile anzeigen
    void updateCsvProgress( int percent ); ///<Signal für die Aktualisierung des Fortschrittsbalkens beim Importieren von CSV-Dateien

public slots:
    void onAppButtonPressed(); ///<Handler für den Aufruf eines Menüpunktes
    void onOpenIndexButtonPressed(); ///<Hadnler für das Öffnen einer index.xml
    void onShowTableInfoButtonPressed(); ///<Handler für die Anzeige der Tabelleninformationen
    void onShowTableDataButtonPressed(); ///<Handler für die Anzeige der Tabellendaten

    /*!
        Fortschrittsbalken für den Import von CSV-Dateien aktualisieren
        \param progress Aktueller Fortschritt in Prozent
    */
    void onUpdateCsvProgress( int progress );

    /*!
        Fortschrittsbalken für den Import von CSV-Dateien anzeigen / ausblenden
        \param progress Bei einem Fortschritt größer 0 Prozent und kleiner 100 Prozent, wird der Fortschrittsbalken angezeigt
    */
    void onProgressBarImportValueChanged( int progress );
    void onTableDataBackButton(); ///<Zurück in die Medienübersicht nach Druck auf den Zurück-Button in der Ansicht der Tabellendaten
    void onSettingsBackButton(); ///<Zurück in die Medienübersicht nach Druck auf den Zurück-Button in den Einstellungen
    void onPageSettingsChanged(); ///<Marker setzen, wenn Änderungen in den Einstellungen vorgenommen wurden
    void onSettingsSaveButtonPressed(); ///<Geänderte Einstellungen speichern
    void onSettingsCancelButtonPressed(); ///<Geänderte Einstellungen verwerfen

private:
    GDPdU* gdpdu =  nullptr; ///<Zeiger auf das GDPdU Objekt
    Ui::MainWindow *ui = nullptr; ///<Zeiger auf Ui::MainWindow mit den GUI-Elementen
    MediaModel* mediaTreeModel = nullptr; ///<Zeiger auf das Model für die Darstellung der GDPdU-Strukturen in einer Baumansicht
    CsvTableModel *csvTableModel = nullptr; ///<Zeiger auf das Model für die Darstellung der Inhalte einer GDPdu-Tabelle
    TableStructureModel* tableStructureModel = nullptr;
    bool pageSettingsChanged = false; ///<Marker für Änderungen in den Einstellungen

    QString getSettingsPath() const; ///<Pfad zur Einstellungsdatei holen

    /*!
        Importieren einer CSV-Datei
        \param mediaNo Nummer des einzulesenden Mediums
        \param tableNo Nummer der einzulesenden Tabelle
        \return true, if file was read successfully
    */
    bool readCSVFile( const int mediaNo, const int tableNo );
};
