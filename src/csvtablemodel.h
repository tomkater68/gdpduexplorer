/*
    gdpduexplorer cvstablemodel.h
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <QAbstractTableModel>
#include <QObject>

#include "structures.h"

/*!\brief Model für die Anzeige der in einer CSV-Datei bereitgestellten
Tabellendaten */
class CsvTableModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    /*!
        Objekt initialisieren
        \param parent Zeiger auf das Elternobjekt
    */
    explicit CsvTableModel( QObject *parent = nullptr );

    ~CsvTableModel(); ///<Objekt bereinigen

    /*!
        Gesamtzahl der Zeilen im Model zurückgeben
        \param parent Zeiger auf das QModelIndex Objekt des Elternobjektes (nicht verwendet)
        \return Anzahl der Zeilen
    */
    virtual int rowCount( const QModelIndex& parent = QModelIndex() ) const override;

    /*!
        Anzahl der Spalten im Model
        \param parent Zeiger auf das QModelIndex Objekt des Elternobjektes (nicht verwendet)
        \return Anzahl der Spalten
    */
    virtual int columnCount( const QModelIndex& parent = QModelIndex() ) const override;

    /*!
        Daten an einer bestimmten Position zurückgeben
        \param ModelIndex Index des Datensatzes, der zurückgegeben werden sollen
        \param Role Rolle, deren Daten angezeigt werden sollen
        \return Datensatz als QVariant
    */
    virtual QVariant data( const QModelIndex& index, int role = Qt::DisplayRole ) const override;

    /*!
        Überschrift einer bestimmten Zeile zurückgeben
        \param section Nummer der Spalte, deren Überschrift zurückgegeben werden soll
        \param orientation Ausrichtung der gewünschten Sapltenüberschrift (Qt::horizonzal oder Qt::vertical)
        \return role Rolle, deren Daten angezeigt werden sollen
    */
    virtual QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const override;

    /*!
        Zeiger auf ein Objekt vom Typ Table übergeben, welches die Liste mit den Tabellendaten enthält,
        die dann im Model angezeigt werden
        \param table Zeiger auf das Objekt vom Typ Table
    */
    void setTable( Table *table );

private:
    Table *table = nullptr; ///<Zeiger auf ein Objekt vom Typ Table mit den anzuzeigenden Daten
};
