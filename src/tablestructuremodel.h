/*
    gdpduexplorer tablestructuremodel.h
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <QStandardItemModel>

class GDPdU;

/*!\Model für die Anzeige der Tabellenstruktur eines GDPdU-Satzes in der Treeview
im Hauptfenster */
class TableStructureModel : public QStandardItemModel
{
    Q_OBJECT
public:

    /*!
        Initialisieren des Objektes
        \param parent Zeiger auf das Elternobjekt
    */
    explicit TableStructureModel( GDPdU* gdpdu, QObject *parent = nullptr );

    ~TableStructureModel(); ///<Objekt aufräumen

    /*!
        Initialisieren des Objektes
        \param MediaNo Nummer des Mediums
        \param tableNo Nu
    */
    void updateStructureData( int mediaNo, int tableNo ); ///<Model aktualisieren, nachdem neue Daten eingelesen wurden

private:
    GDPdU* gdpdu = nullptr; ///<Zeiger auf das Objekt vom Typ GDPdU

    void clearModel(); ///<Vorhandene Daten im Model löschen
};
