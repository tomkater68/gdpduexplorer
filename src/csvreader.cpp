/*
    gdpduexplorer cvsreader.cpp
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "csvreader.h"
#include "backend.h"
#include "gdpdu.h"
#include "structures.h"

#include <QFile>
#include <QStringConverter>
#include <QTextStream>
#include <QCoreApplication>
#include <QApplication>
#include <QDate>

CsvReader::CsvReader(Backend* backend, GDPdU* gdpdu)
    : QObject{backend}
{

    if ( backend == nullptr )
        qFatal( "CsvReader::CsvReader - Backend == nullptr" );

    if ( gdpdu == nullptr )
        qFatal( "CsvReader::CsvReader - GDPdU == nullptr" );

    this->backend = backend;
    this->gdpdu = gdpdu;

    errorHandling = backend->getParameter( "Import/ErrorHandling", 1 ).toInt();
}

bool CsvReader::readCSV( int mediaIndex, int tableIndex ) {

    backend->enableMainMenu( false );
    table = getTableFromIndex( mediaIndex, tableIndex );

    if ( table == nullptr )
        qFatal( "CsvReader::readCSV - Table == nullptr" );

    if ( table->imported ) {
        backend->enableMainMenu( true );
        return true;
    }

    qInfo().nospace() << "Importing file: " << qPrintable( table->urlFullPath );

    QFile csvFile( table->urlFullPath );

    if ( !csvFile.exists() ) {

        qCritical().nospace() << "GDPdU error. File not found: " << qPrintable( table->urlFullPath );
        emit backend->showMessageDialog( tr("Error reading csv file"), tr( "File %1 not found!" ).arg( table->urlFullPath ) );
        return false;
    }

    if ( !csvFile.open( QIODevice::ReadOnly ) ) {

        qCritical().nospace() << "GDPdU error. Unable to open file:  " << qPrintable( table->urlFullPath );
        emit backend->showMessageDialog( tr("Error reading csv file"), tr( "Unable to open file %1!" ).arg( table->urlFullPath ) );
        return false;
    }

    if ( table->codec == Table::eUTF7 || table->codec == Table::eMacintosh || table->codec == Table::eOEM ) {

        qWarning().nospace() << "GDPdU error. Unsupported encoding" << qPrintable( QString::number( table->codec ) );
        emit backend->showMessageDialog( tr("Error reading csv file"), tr( "Nicht unterstützte Zeichensatzkodierung." ) );
        return false;
    }

    QCoreApplication::processEvents();
    QApplication::setOverrideCursor(Qt::WaitCursor);

    QTextStream in( &csvFile );

    switch ( table->codec ) {

    case Table::eUTF8:
        in.setEncoding( QStringConverter::Utf8 );
        break;

    case Table::eUTF16:
        in.setEncoding( QStringConverter::Utf16 );
        break;

    case Table::eANSI:
        in.setEncoding( QStringConverter::Latin1 );
        break;
    }

    qint64 fileSize = csvFile.size();
    qint64 bytesRead = 0;

    qDebug().nospace() << "File size:  " << qPrintable( QString::number( fileSize ) );

    if ( table->skipBytes > 0 ) {
        in.read( table->skipBytes );
        bytesRead = bytesRead + table->skipBytes;

        qDebug().nospace() << "Skipped " << qPrintable( QString::number( table->skipBytes ) ) << "bytes";
    }

    QString fileInput;

    qDebug().nospace() << "Start reading file";

    int buffer = backend->getParameter( "Import/Buffer", 2048 ).toInt();

    if ( buffer < 1024 )
        buffer = 1024;

    while ( !in.atEnd() && !cancelImport )
    {
        QString temp = in.read( buffer );
        fileInput += temp;
        bytesRead = bytesRead + temp.length();

        emit backend->updateCsvProgress( bytesRead * 100 / fileSize );

        if ( fileInput.contains( table->recordDelimiter ) ) {
            QStringList records;
            records = fileInput.split( table->recordDelimiter );

            for ( int i = 0; i < records.size() - 1; i ++ ) {
                processRecord( records.at( 0 ) );
                rowCount = rowCount + 1;
            }

            fileInput = records.last();
        }

        QCoreApplication::processEvents();
    }

    QApplication::restoreOverrideCursor();
    backend->enableMainMenu( true );

    if ( cancelImport ) {
        qDebug().nospace() << "CSV import canceled due to errors";

        if ( table->columnValueList.size() ) {

            for ( int i = 0; i < table->columnValueList.size(); i++ ) {
                QVector<QString> column = table->columnValueList.at( i );
                column.clear();
            }
            table->columnValueList.clear();
        }
    }
    else {
        qDebug().nospace() << "Finished reading file";

        table->imported = true;

        if ( errorCount > 0 )
            emit backend->showMessageDialog( tr( "Import finished with errors" ) , tr( "CSV import finished with errors. %1 records rejected" ).arg( errorCount ) );
    }

    return !cancelImport;
}

Table* CsvReader::getTableFromIndex( int mediaIndex, int tableIndex ) {

    return gdpdu->getMediaList()->at( mediaIndex )->tableList.at( tableIndex );
}

void CsvReader::processRecord( QString record ) {

    if ( table->isFixedLength )
        processFixedLength( record );
    else
        processVariableLength( record );
}

void CsvReader::processFixedLength( QString record ) {

    Q_UNUSED( record );
}

void CsvReader::processVariableLength( QString record ) {

    QStringList columns = record.split( table->columnDelimiter );

    if ( columns.size() != table->columnList.size() ) {
        errorCount = errorCount + 1;

        qCritical().nospace() << "CSV column count doesn't match column count in index.xml";
        qCritical().nospace() << "Error reading CSV file. Row: " << qPrintable( QString::number( rowCount ) );

        if ( errorHandling == 0 )
            cancelImport = true;

        return;
    }

    QVector<QString> columnValues;
    for ( int i = 0; i < columns.size(); i++ )
    {
        QString value = columns.at( i );

        switch ( table->columnList.at( i )->type )
        {
        case Column::eAlphanumeric:
            value = formatString( i, value );
            break;

        case Column::eNumeric:
            value = formatNumeric( i, value );
            break;

        case Column::eDate:
            value = formatDate( i, value );
            break;
        }

        columnValues.push_back( value );
    }

    table->columnValueList.push_back( columnValues );
}

QString CsvReader::formatString( int column, QString value )
{
    /* Marker für Textanfang/Textende entfernen */
    value = value.remove( table->textEncapsulator );

    /* ggf. Textlänge anpassen */
    int maxLength = table->columnList.at( column )->maxLength;

    if ( maxLength > 0 )
        value = value.mid( 0, maxLength );

    /* ggf. Wertetabellen auflösen */
    if ( table->columnList.at( column )->hasMap )
    {
        for( auto it: table->columnList.at( column )->mapList )
        {
            if ( it->from == value )
            {
                value = it->to;
                break;
            }
        }
    }
    return value;
}

QString CsvReader::formatDate( int column, QString value )
{
    QDate dtValue;
    QString dateFormat = table->columnList.at( column )->dateFormat;

    if ( dateFormat.contains( "yyyy") )
        dtValue = QDate::fromString( value, dateFormat );
    else
    {
        int posYear = dateFormat.indexOf( "y" );
        int posMonth = dateFormat.indexOf( "M" );
        int posDay = dateFormat.indexOf( "d" );

        int year = value.mid( posYear, 2 ).toInt();
        if ( year < table->columnList.at( column )->epoch )
            year += 2000;
        else
            year += 1900;

        dtValue = QDate( year, value.mid( posMonth, 2 ).toInt(), value.mid( posDay, 2 ).toInt() );
    }

    if ( !dtValue.isValid() )
        return QString();

    return QString( "%1" ).arg( dtValue.toString( "dd.MM.yyyy" ) );
    return value;
}

QString CsvReader::formatNumeric( int column, QString value )
{
    QLocale locale = QLocale::system();

    locale.setNumberOptions ( QLocale::OmitGroupSeparator );

    value = value.replace( table->decimalSymbol, locale.decimalPoint() );

    value.remove( table->digitGrouping );

    double dValue = locale.toDouble( value );

    int impliedAccuracy = table->columnList.at( column )->impliedAccuracy;
    if ( impliedAccuracy > 0 )
        dValue = dValue / ( 10 ^ impliedAccuracy );

    QString cValue;

    int accuracy = table->columnList.at( column )->accuracy;

    if ( accuracy > 0 )
        cValue = locale.toString ( dValue,'f', accuracy );
    else if ( impliedAccuracy > 0 )
        cValue = locale.toString ( dValue,'f', impliedAccuracy );
    else
        cValue = locale.toString ( dValue,'f', 0 );

    return cValue;
}
