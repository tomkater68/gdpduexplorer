/*
    gdpduexplorer mainwindow.h
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QMainWindow>

class Backend;

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

/*!\Hauptfenster mit dem Menü in der Seitenleiste und den Programmfenstern, die in einem Widgetstack
organisiert sind */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*!
        Hauptfenster initalisieren
        \param Zeiger auf das Elternobjekt
    */
    MainWindow( QWidget *parent = nullptr );
    ~MainWindow(); ///<Objekt beim Beenden bereinigen

public slots:

    /*!
        GUI Elemente nach dem Wechsel der Seite im Widgetstack anpassen
        \param page der aufzurufenden Seite
    */
    void onWidgetStackPageChanged( int page );

    /*!
        Meldung in der Statuszeile anzeigen
        \param page der aufzurufenden Seite
    */
    void onShowStatusMessage( const QString& message );

protected:
    /*!
        Vor dem Schließen des Hauptfensters einen Abfragedialog anzeigen
        \param event Zeiger auf das Event vom Typ QCloseEvent
    */
    void closeEvent ( QCloseEvent *event );

private:
    Ui::MainWindow *ui; ///<Zeiger auf das UI-Objekt mit den GUI-Elementen
    Backend* backend = nullptr; ///<Zeiger auf das Backend
};

