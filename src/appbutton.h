/*
    gdpduexplorer appbutton.h
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <QFrame>

/*!\In der Seitenleiste des Hauptfensters werden Schalter mit Icon und Text verwendet, die von
QFrame abgeleitet wurden. Da QFrame kein Maushandling enthält, wird dieses hier nachgerüstet. */
class AppButton : public QFrame
{
    Q_OBJECT

public:
    /*!
        Initialisieren des Objektes
        \param parent Zeiger auf das Elternobjekt
        \param f Flags für das Fenster-Design
    */
    AppButton( QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags() );

signals:
    void pressed(); ///<Signal wird ausgelöst, wenn die Maustaste gedrückt wurde

protected:
    /*!
        Abfangen des Maus-Events
        \param event pointer to QMouseEvent
    */
    void mousePressEvent( QMouseEvent* event );
};
