/*
    gdpduexplorer main.cpp
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"

#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <QFontDatabase>
#include <QFile>
#include <QDate>
#include <QDir>

static QTextStream output_ts;

/* Implementation messagehandler */
void MessageOutput( QtMsgType type, const QMessageLogContext &context, const QString &msg )
{
    Q_UNUSED( context );

    switch (type) {
    case QtDebugMsg:
        output_ts << QString( "%1|%2|Debug|%3" ).arg( QDate::currentDate().toString( "yyyy-MM-dd" ), QTime::currentTime().toString( "hh:mm:ss" ), msg) << Qt::endl;
        break;
    case QtInfoMsg:
        output_ts << QString( "%1|%2|Info|%3" ).arg( QDate::currentDate().toString( "yyyy-MM-dd" ), QTime::currentTime().toString( "hh:mm:ss" ), msg) << Qt::endl;
        break;
    case QtWarningMsg:
        output_ts << QString( "%1|%2|Warning|%3" ).arg( QDate::currentDate().toString( "yyyy-MM-dd" ), QTime::currentTime().toString( "hh:mm:ss" ), msg) << Qt::endl;
        break;
    case QtCriticalMsg:
        output_ts << QString( "%1|%2|Critical|%3" ).arg( QDate::currentDate().toString( "yyyy-MM-dd" ), QTime::currentTime().toString( "hh:mm:ss" ), msg) << Qt::endl;
        break;
    case QtFatalMsg:
        output_ts << QString( "%1|%2|Fatal|%3" ).arg( QDate::currentDate().toString( "yyyy-MM-dd" ), QTime::currentTime().toString( "hh:mm:ss" ), msg) << Qt::endl;
        abort();
    }
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    app.setWindowIcon( QIcon( ":/applogo.png" ) );
    app.setApplicationName( "gdpduexplorer" );
    app.setApplicationVersion( "0.1.0" );

    QFontDatabase::addApplicationFont( ":/fontello.ttf" );

    //Install MessageHandler
    QString logfilePath = QString( "%1/.gdpduexplorer/gdpduexplorer-%2.log" ).arg( QDir::homePath(), QDate::currentDate().toString( "yyyy-MM-dd" )  );

    QFile debugFile( logfilePath );

    if ( debugFile.exists() )
        debugFile.open( QIODevice::Append | QIODevice::Text );
    else
        debugFile.open(QIODevice::WriteOnly | QIODevice::Append);

    output_ts.setDevice( &debugFile);
    qInstallMessageHandler( MessageOutput );

    qInfo( "Starting application" );
    qInfo().nospace() << app.applicationVersion();

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "gdpduexplorer_" + QLocale(locale).name();
        if (translator.load(":/" + baseName)) {
            app.installTranslator(&translator);
            qInfo().nospace() << "Loaded application language " << qPrintable( QLocale( locale ).name() );
            break;
        }
    }

    QTranslator qtTranslator;
    for ( const QString &locale : uiLanguages ) {
        const QString baseName = "qt_" + QLocale( locale ).name();
        if ( qtTranslator.load( ":/" + baseName ) ) {
            //qInfo().nospace() << "Loading library language " << qPrintable( QLocale( locale ).name() );
            app.installTranslator( &qtTranslator );
            qInfo().nospace() << "Loaded library language " << qPrintable( QLocale( locale ).name() );
            break;
        }
    }

    MainWindow w;
    w.show();

    int result = app.exec();

    qInfo( "Shutting down application" );
    qInfo( "------------------------" );

    return result;
}
