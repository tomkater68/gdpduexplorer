/*
    gdpduexplorer appbutton.h
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mediamodel.h"
#include "gdpdu.h"

MediaModel::MediaModel( GDPdU *gdpdu, QObject *parent )
    : QStandardItemModel{parent}
{
    this->gdpdu = gdpdu;

    if ( this->gdpdu == nullptr )
        qFatal( "MediaModel::MediaModel - gdpdu == nullptr" );

    QObject::connect( gdpdu, &GDPdU::mediaListUpdated, this, &MediaModel::updateMediaData );
}

MediaModel::~MediaModel()
{
    //Die QStandardItems werden von Qt beseitigt, daher muss hier nur die Mapping-Tabelle gelöscht werden
    for( auto key: itemMap.keys() )
        delete itemMap.value( key );

    itemMap.clear();
}

void MediaModel::clearModel()
{
    removeRows( 0, rowCount() );

    for( auto key: itemMap.keys() )
        delete itemMap.value( key );

    itemMap.clear();
}

int MediaModel::getMediaNumberByItem( QStandardItem* item )
{
    TreeItem* treeItem = itemMap.value( item , nullptr );

    if ( treeItem == nullptr )
        return -1;

    return treeItem->media;
}

int MediaModel::getTableNumberByItem( QStandardItem* item )
{
    TreeItem* treeItem = itemMap.value( item , nullptr );

    if ( treeItem == nullptr )
        return -1;

    return treeItem->table;
}

void MediaModel::updateMediaData()
{
    clearModel();

    for ( int i = 0; i < gdpdu->getMediaList()->size(); i ++ )
    {
        Media* medium = gdpdu->getMediaList()->at( i );

        QStandardItem* itemMedia = new QStandardItem(QIcon( ":/icon-media.png" ), medium->name );

        TreeItem* item = new TreeItem;
        item->media = medium->mediaNo;
        itemMap.insert( itemMedia, item );
        appendRow( itemMedia );

        for ( int i2 = 0; i2 < medium->tableList.size(); i2 ++ )
        {
            Table* table = medium->tableList.at( i2 );

            QStandardItem* itemTable = new QStandardItem(QIcon(":/icon-table.png"), table->tableName );
            item = new TreeItem;
            item->media = medium->mediaNo;
            item->table = table->tableNo;
            itemMap.insert( itemTable, item );
            itemMedia->appendRow( itemTable );

            QStandardItem* itemUrl = new QStandardItem(QIcon(":/icon-url.png"), table->url );
            QFont font = itemUrl->font();
            font.setItalic( true );
            itemUrl->setFont( font );

            item = new TreeItem;
            item->media = medium->mediaNo;
            item->table = table->tableNo;
            itemMap.insert( itemUrl, item );
            itemTable->appendRow( itemUrl );

            QStandardItem* itemDesc = new QStandardItem(QIcon(":/icon-text.png"), table->tableDesc );
            item = new TreeItem;
            item->media = medium->mediaNo;
            item->table = table->tableNo;
            itemMap.insert( itemDesc, item );
            itemTable->appendRow( itemDesc );

            QStandardItem* itemRange = new QStandardItem(QIcon(":/icon-date.png"), table->dateFormatted );
            item = new TreeItem;
            item->media = medium->mediaNo;
            item->table = table->tableNo;
            itemMap.insert( itemRange, item );
            itemTable->appendRow( itemRange );
        }
    }
}
