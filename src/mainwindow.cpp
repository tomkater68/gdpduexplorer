/*
    gdpduexplorer mainwindow.cpp
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include "backend.h"
#include "gdpdu.h"
#include "./ui_mainwindow.h"

#include <QFileDialog>
#include <QCloseEvent>
#include <QFileInfo>
#include <QStandardItemModel>

MainWindow::MainWindow( QWidget *parent )
    : QMainWindow( parent )
    , ui( new Ui::MainWindow )
{
    ui->setupUi( this );
    setWindowTitle( tr( "gdpduexplorer" ) );

    backend = new Backend( ui, this );

    if ( backend == nullptr )
        qFatal( "MainWindow::MainWindow - backend == nullptr" );

    QObject::connect( ui->stackedWidget, &QStackedWidget::currentChanged, this, &MainWindow::onWidgetStackPageChanged );
    QObject::connect( backend, &Backend::showStatusMessage, this, &MainWindow::onShowStatusMessage );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onShowStatusMessage( const QString& message ) {

    statusBar()->showMessage( message, 3000 );
}

void MainWindow::onWidgetStackPageChanged( int page )
{
    QString windowTitle = "gdpduexplorer - ";

    switch (page) {
    case Pages::ePageExplore:
        windowTitle += tr( "Explore" );
        break;

    case Pages::ePageSettings:
        windowTitle += tr( "Settings" );
        break;

    case Pages::ePageTableData:
        windowTitle += tr( "View table data" );
        break;

    case Pages::ePageTableInformation:
        windowTitle += tr( "View table structure" );
        break;
    }

    setWindowTitle( windowTitle );
}

void MainWindow::closeEvent( QCloseEvent *event )
{
    if ( backend->yesNoDialog( tr( "Do you really want to quit the application?" ) ) )
        event->accept();
    else
        event->ignore();
}


