/*
    gdpduexplorer gdpdu.h
    Copyright (C) 2024  Michael Saalfeld <tomkater@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <QObject>
#include <QVector>

#include "structures.h"

class Backend;
class XmlReader;

/*!\brief Read and store GDPdU structure */
class GDPdU : public QObject
{
    Q_OBJECT
public:
    /*!
        Objekt initialisieren
        \param parent Zeiger auf das Elternobjekt
    */
    explicit GDPdU( Backend* backend );

    ~GDPdU(); ///<Objekt beim Beenden bereinigen

    QString getSupplier(); ///<Name und Ort des Datenlieferanten holen
    QString getDescription(); ///<Beschreibung der Daten holen

    /*!
        Beschreibungsdatei index.xml lesen
        \param fileName Referenz auf den zu lesenden Dateinamen
    */
    void readXMLFile( QString& fileName );

    /*!
        Medium zur Medienliste hinzufügen
        \return Zeiger auf das Objekt vom Typ Media
    */
    Media* addMedia();

    /*!
        Name des Datenlieferanten setzen
        \param Name des Datenlieferanten
    */
    void setSupplierName( const QString& name );

    /*!
        Ort setzen
        \param Name des Ortes
    */
    void setLocation( const QString& location );

    /*!
        Datenbeschreibung setzen
        \param Beschreibung der Daten
    */
    void setDescription( const QString& description );

    /*!
        Zeiger auf die Medienliste holeb
        \return Zeiger auf einen Vektor vom Typ Media
    */
    QVector<Media*>* getMediaList();

    int getMediaCount(); ///<Anzahl der Medien im Datensatz holen

    int getTableCount(); ///<Anzahl der Tabellen im Datensatz holen

signals:
    void mediaListCleared(); ///Signal. dass die Medienliste geleert wurde. Wird vom Datenmodell benötigt
    void mediaListUpdated(); ///<Signal. dass die Medienliste aktualisiert wurde. Wird vom Datenmodell benötigt

private:
    QString xmlFileName; ///<Name der Datenbeschreibungsdatei (index.xml)
    QString supplier; ///<Name des Datenlieferanten
    QString location; ///<Ort des Datenlieferanten
    QString description; ///<Beschreibung der Daten
    QString path; ///<Pfad zur index.xml. Wird für die Auflösung relativer Pfade benötigt
    QString media; ///<last read media

    Backend* backend = nullptr; ///<Zeiger auf das Backend Objekt
    XmlReader* reader = nullptr; ///<Zeiger auf das XmlReader Objekt

    QVector<Media*> mediaList; ///<Liste der im Datensatz enthaltenen Medien

    void clearMediaList(); ///<Medienliste leeren
};
