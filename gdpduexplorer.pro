QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/main.cpp\
    src/mainwindow.cpp\
    src/appbutton.cpp\
    src/backend.cpp\
    src/aboutdialog.cpp\
    src/gdpdu.cpp\
    src/xmlreader.cpp\
    src/mediamodel.cpp\
    src/csvreader.cpp\
    src/csvtablemodel.cpp\ 
	src/tablestructuremodel.cpp

HEADERS += \
    src/mainwindow.h\
    src/appbutton.h\
    src/backend.h\
    src/aboutdialog.h\
    src/structures.h\
    src/gdpdu.h\
    src/xmlreader.h\
    src/csvreader.h\
    src/mediamodel.h\
    src/csvreader.h\
    src/csvtablemodel.h\
	src/tablestructuremodel.h

FORMS += \
    src/ui/mainwindow.ui\
    src/ui/aboutdialog.ui

TRANSLATIONS += \
    src/resources/translation/gdpduexplorer_de_DE.ts
CONFIG += lrelease
CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
